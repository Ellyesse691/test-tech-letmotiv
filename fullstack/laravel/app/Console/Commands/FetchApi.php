<?php

namespace App\Console\Commands;

use App\Models\Character;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class FetchApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fetch-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commande de récupération des données depuis l\'API';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $response = Http::get('https://rickandmortyapi.com/api/character', [
            'limit' => 100, 
        ]);
        if ($response->successful()) {
            $data = $response->json();
            // echo $data ['results'][0]['origin']['name'];
            // echo count($data['results'][0]['episode']);

            foreach ($data['results'] as $characterData) {
                $character = new Character();
                $character->picture_url = $characterData['image'];
                $character->last_name = $characterData['name'];
                $character->first_name = $characterData['name'];
                $character->species = $characterData['species'];
                $character->gender = $characterData['gender'];
                $character->status = $characterData['status'];
                $character->origin = $characterData['origin']['name'];
                $character->episodes = count($characterData['episode']); 
                $character->save();
            }
        } else {
            $this->error('Erreur lors de la récupération des données depuis l API.');
        }
    }
}
