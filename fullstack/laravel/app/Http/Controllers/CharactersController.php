<?php

namespace App\Http\Controllers;
use App\Models\Character;


class CharactersController extends Controller
{
    public function index(){

           return view('characters');
       }


       public function getAll(){
        $characters = Character::where('gender', 'female')
           ->where('species', 'human')
           ->get();
           return response()->json($characters);
       }

}